<!DOCTYPE html>
<html>
    @include('partials.head')
    <body class="page-index">
    @include ('partials.message')
        <div class="container">
            <header class="mainHeader">
                <div class="wrapper flex">
                    @include('partials.navbar')

                </div>
            </header>
            <section class="mainContent">
                @yield('content')
            </section>
            @include('partials.footer')
        </div>

        @auth
        <form id="logout-form" method="POST" action="{{ route('logout') }}">
            @csrf
        </form>

        <script>
            document.querySelector("a[href='#logout']").addEventListener('click', e => {
                e.preventDefault();

                document.querySelector('#logout-form').submit();
            }, false);
        </script>
        @endauth
        <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous">
        </script>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>

