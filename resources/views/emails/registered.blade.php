@component('mail::message')
Hi, {{ auth()->user()->name }}!

Thank You for registering on our blog!

You now have access to all premium content on
@component('mail::button', ['url' => url('/') ])
    LaraBlogger
@endcomponent

Thanks, </br>
{{ config('app.name') }}
@endcomponent
