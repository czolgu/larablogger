@component('mail::message')
Hello!

User sent you a message: <br>
<hr>
<strong>{{ $contactFormData['subject'] }}</strong> <br>
<hr>
{{ $contactFormData['message'] }} <br>


User's mail: {{ $contactFormData['email'] }}

Thanks, <br>
{{ config('app.name') }}
@endcomponent