@extends('layouts.master')
@section('title', 'Sign in')

@section('content')
    <div class="wrapper">
        <div class="rte">
            <h1>Login</h1>
        </div>

        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-fieldset {{ $errors->has('email') ? ' is-invalid' : '' }}">
                <input class="form-field is-invalid" type="email" name="email" placeholder="Your e-mail" value="{{ old('email') }}">
            </div>
            <div class="form-fieldset {{ $errors->has('password') ? ' is-invalid' : '' }}">
                <input class="form-field" type="password" name="password" placeholder="Password" value="{{ old('password') }}">
            </div>
            <button class="button">Submit</button>
        </form>

        <div class="rte mt">
            <p>Don't have an account? <a href="{{ route('register') }}">Register now.</a><br>Forgot your password? <a href="{{ route('password.request') }}">Reset it here.</a></p>
        </div>
    </div>
@endsection

