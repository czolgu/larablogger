<footer class="mainFooter">
    <div class="wrapper">
        <p>&copy; {{ date('Y') }} LaraBlogger</p>
        <nav>
            <ul>
                <li><a href=" {{ route('about') }} ">About me</a></li>
                <li><a href="#">Login</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">RSS</a></li>
            </ul>
        </nav>
        <p class="author">All rights reserved <a href="{{ url('/') }}">LaraBlogger</a></p>
    </div>
</footer>
