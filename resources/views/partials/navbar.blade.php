<a href="{{ url('/') }}" class="logo">LaraBlogger</a>
<nav>
    @if (is_null(Auth::user()) === false)
        @if (Auth::user()->hasVerifiedEmail() === false)
            <p style="color:aliceblue">You are not verified</p>
        @else
            <p style="color:aliceblue">You are verified</p>
        @endif
    @endif
    <ul>
        @auth
            <li><a href="{{ route('about') }}"{!! request()->routeIs('about') === true ? ' class="is-active"' : '' !!}>About me</a></li>
            <li><a href="#logout">Logout</a></li>
            @can('manage-posts')
            <li><a href="{{ route('admin.post.create') }}">Create a post</a></li>
            @endcan
        @else
            <li><a href="{{ route('login') }}"{!! request()->routeIs('login') === true ? ' class="is-active"' : '' !!}>Log in</a></li>
        @endauth
        <li><a href="{{ route('contact.index') }}">Contact</a></li>
        <li><a href="{{ route('feeds.main') }}">RSS <i class="fa fa-rss-square"></i></a></li>
    </ul>
</nav>
<form method="GET" action="{{ route('search') }}" class="search">
    <div class="form-fieldset">
        <input type="text" name="q" class="form-field search-input" placeholder="Search...">
    </div>
</form>

