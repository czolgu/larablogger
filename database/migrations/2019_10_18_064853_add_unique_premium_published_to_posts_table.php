<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniquePremiumPublishedToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            // while changing existing column on a table you need to call 'change()' method on the changed definition of a column
            $table->string('title')->unique()->change();
            $table->boolean('published')->default(false)->after('content');
            $table->boolean('premium')->default(false)->after('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('title')->change();
            $table->dropColumn('published');
            $table->dropColumn('premium');
        });
    }
}
