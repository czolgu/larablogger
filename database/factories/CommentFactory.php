<?php

/** @var Factory $factory */
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 4),
        'post_id' => rand(1, 10),
        'date' => now(),
        'content' => $faker->paragraph(rand(10, 20)),
    ];
});
