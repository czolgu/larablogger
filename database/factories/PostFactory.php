<?php

/** @var Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => 4,
        'title' => $faker->sentence(5),
        'content' => $faker->paragraph(14),
        'published' => rand(0, 1),
        'premium' => rand(0, 1),
        'date' => now(),
        'type' => 'text',
    ];
});

$factory->state(Post::class, 'image', function (Faker $faker) {
    return [
        'content' => null,
        'type' => 'photo',
        'image' => $faker->imageUrl(1200, 800),
    ];
});
