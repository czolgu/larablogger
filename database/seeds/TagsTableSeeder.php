<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    public const NUMBER_OF_TAGS = 13;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory = factory(App\Models\Tag::class);

        $tag = factory(App\Models\Tag::class)->create();

        foreach (range(1, TagsTableSeeder::NUMBER_OF_TAGS) as $i) {
            $factory = factory(App\Models\Tag::class);

            $postId = rand(1, PostsTableSeeder::NUMBER_OF_POSTS);
            $post = App\Models\Post::findOrFail($postId);

            $tag = $factory->create();

            $post->tags()->attach($tag->id);
        }
    }


}
