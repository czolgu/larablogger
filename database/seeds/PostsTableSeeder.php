<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    public const NUMBER_OF_POSTS = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, PostsTableSeeder::NUMBER_OF_POSTS) as $i) {
            $factory = factory(App\Models\Post::class);

            if (rand(1, 6) === 1) {
               $factory->state('image');
            }

            $factory->create();
        }
    }
}
