<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory = factory(App\User::class);

        $factory->create();
        $factory->create();
        $factory->create();
        $factory->state('admin')->create();
    }
}
