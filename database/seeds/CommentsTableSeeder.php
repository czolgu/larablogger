<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    public const NUMBER_OF_COMMENTS = 6;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Models\Comment::class, CommentsTableSeeder::NUMBER_OF_COMMENTS)->create();
    }
}
