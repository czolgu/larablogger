<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PostController@index')->name('posts');

Route::get('/mail', function() {
    $user = App\User::first();

    return new App\Mail\UserRegistered($user);
});

Route::get('/contact', 'ContactController@index')->name('contact.index');
Route::post('/contact', 'ContactController@send')->name('contact.send');
Route::get('/search', 'SearchController@index')->name('search');

//Route::get('/posts/{id}', 'PostController@show')->name('posts.show');

// instead of putting {id} in route signature
// we can put {post} in the route, so when we give 'posts/1' url to laravel, it will look for post of id: 1
//Route::get('/posts/{post}', 'PostController@show')->name('posts.show');

// we will retrieve posts with slug instead of post's id
Route::get('/posts/{slug}', 'PostController@show')->name('posts.show');
Route::get('/tag/{slug}', 'TagController@show')->name('tag.show');

Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('/email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::get('/account/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/account/register', 'Auth\RegisterController@register');

Route::get('/account/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/account/login', 'Auth\LoginController@login');
Route::post('/account/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::group(['prefix' => 'admin', 'middleware' => ['can:manage-posts', 'auth']], function() {
    Route::get('/post/create', 'Admin\PostController@create')->name('admin.post.create');
    Route::post('/post/create', 'Admin\PostController@store')->name('admin.post.store');
    Route::get('/post/{id}', 'Admin\PostController@edit')->name('admin.post.edit');
    Route::put('/post/{id}', 'Admin\PostController@update')->name('admin.post.update');
    Route::delete('/post/{id}', 'Admin\PostController@destroy')->name('admin.post.delete');
});

Route::post('/comment/create', 'CommentController@store')->name('comment.create');

Route::feeds();




