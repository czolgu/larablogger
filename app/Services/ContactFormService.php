<?php

namespace App\Services;

use App\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Types\Array_;

class ContactFormService {
    public function sendContactEmail($contactFormData) {
        Mail::to(config('mail.admin.address'))
            ->send(new ContactForm($contactFormData));
    }
}