<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Post extends Model implements Feedable
{
    protected $table = 'posts';

    protected $fillable = [
        'user_id',
        'title',
        'content',
        'date',
        'type',
        'image',
        'published',
        'premium',
    ];

    protected $dates = [
        'date'
    ];

    public function setTypeAttribute($value) {
        $this->attributes['type'] = $value === 'text' ? 0 : 1;
    }

    public function setDateAttribute($value) {
        $this->attributes['date'] = is_null($value) === true ? now() : $value;
    }

    // always when we will set 'title' attribute on Post model this method will run
    public function setTitleAttribute($value) {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    // method that will run when we try to read an attribute
    public function getTitleAttribute() {
        return $this->attributes['title'];
    }

    // setting which column laravel will look at when getting data under /posts/{post} route,
    // in this case it will look for posts that will have value of 'slug' column equal to value under {post} placeholder
    public function getRouteKeyName() {
        return 'slug';
    }

    // after creating this method we can get data it returns with $post->excerpt
    public function getExcerptAttribute() {
        return Str::limit(strip_tags($this->content), 300);
    }

    public function getTagsListAttribute() {
//        $tagsString = '';
//
//        $tags = $this->tags()->get();
//
//        foreach($tags as $tag) {
//            $tagsString .= "{$tag->name} ";
//        }
//
//        return $tagsString;
        $tags = $this->tags()->get();

        $tagsString = $tags->implode('name', ' ');

        return $tagsString;
    }

    public function getPhotoAttribute() {
        // we now have two types of images in the db: local images, uploaded on our server by users and
        // images hosted outside of our architecture
        return Str::startsWith($this->image, 'http') === true ? $this->image : Storage::url($this->image);
    }

    // receives query when using this model somewhere in the app, can modify it. In this case: Post::published()->... will
    // only return objects with 'published' field set to 1.
    public function scopePublished($query) {
        $user = auth()->user();

        // checking if user is logged in and if hes an admin
        if (is_null($user) === false && $user->isAdmin() === true) {
            return $query;
        }

        if (is_null($user) === true) {
            $query->where('premium', '<>', 1);
        }

        return $query->where('published', 1);
    }

//    public function user() {
//        return $this->belongsTo(User::class);
//    }
    // this will allow us to find this posts owner, laravel will, by default, search for 'user_id', because the model its
    // related to is called User. If foreign key's name is not this, you need to specify it by:

    public function author() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    // because the method's name is author, laravel will look for author_id column in posts table. It will not find it as we have user_id
    // column instead. Thats when we can specify the foreign key and owner key as shown above

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
    // $post->tags()->attach($tagId) will attach a tag to a post later on
    // $post->tags()->get() will retrieve all the tags attached to this post
    // $post->tags()->find($id) will look for tags of specific id attached to this post

    public function toFeedItem() {
        return FeedItem::create([
            'id' => $this->id,
            'title' => $this->title,
            'summary' => $this->excerpt,
            'updated' => $this->updated_at,
            'link' => route('posts.show', $this->slug),
            'author' => $this->author->name,
        ]);
    }

    public static function getFeedItems() {
        return Post::published()->get();
    }
}
