<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\TagsParsingService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

use App\Models\Post;

class PostController extends Controller
{
    public function __construct() {
        // we can define middlewares on this controller and specific methods here or in web.php file, in web.php file we can
        // group the routes and define middlewares that will be used on entire groups of routes

        // the code below will only alow users that are logged in to call any action in this controller unless we put specific
        // methods in 'except' method on this middleware

        //$this->middleware('auth');

        // the code below is using middleware - a gate that is named 'manage-posts'. We register
        // gates in AuthServiceProvider.php. This particular gate is checking if user is an admin. Only then
        // the user can use methods in this controller only when the middlewares registered here, in constructor are all passed.
        // we can exclude some methods in this class from being guarded with any of the middlewares/gates by saying:
        // $this->middleware('can:manage-posts')->except('create');
        // the code above indicates, that user trying to use method 'create' does not have to possess can:manage-posts privilage
        //$this->middleware('can:manage-posts')->except('create');
    }

    protected function validator($data) {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'type' => ['required', Rule::in(['text', 'photo'])],
            'date' => 'nullable|date',
            'tags' => 'nullable',
            'image' => 'nullable|image|max:1024',
            'content' => 'nullable',
            'published' => 'boolean',
            'premium' => 'boolean',
        ])->validate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $data = $this->validator($request->all());

        // the following code will run ONLY AFTER the validation has been passed, no need to do any checks here
        $userId = auth()->user()->id;
        $data['user_id'] = $userId;

        if (isset($data['image']) === true) {
            // 'image' is the name of the field input in which we stored a file
            // 'photos' is the name of the directory in which we will store given file: from config/filesystems.php: storage/app/public,
            // in this case it will be: storage/app/public/photos, as we called the dir this way
            $path = $request->file('image')->store('photos');
            $data['image'] = $path;
        }

        //        // the code below will create a post and save it automatically
//        Post::create([/*dane do utworzenia posta*/]);
//
//        // the code below will create Post model instance, will not save it in the db automatically, you need to call
//        // save() method on the instance in order to save it in the db
//        $post = new Post([/*dane do utworzenia posta*/]);

        /**
         * @var Post $post
         */
        $post = Post::create($data);

        if (isset($data['tags']) === true) {
            // this method will get the string with tags, example: '#style #life #adventures' and split those by whitespaces,
            // create a tag instance out of each one of those.
            $tagIds = TagsParsingService::parse($data['tags']);

            // this code will get all tags in relation with this post and replace them all with new tags. if tags was an array:
            // [1, 2] and we gave it [1], it would just remove '2' from the array
            $post->tags()->sync($tagIds);
        }

        session()->flash('message', 'Post has been added!');

        return redirect(route('posts.show', $post->slug));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {
        /**
         * @var Post $post
         */
        $post = Post::findOrFail($id);

        return view('admin.post.edit', compact('post'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, $id)
    {
        // the code below will only allow users that pass 'manage-posts' gate to call this method
        // $this->authorize('manage-posts');

        /**
         * @var Post $post
         */
        $post = Post::findOrFail($id);
        $oldImage = $post->image;

        $requestData = $request->all();

        // browser is simply not sending form fields which are checkboxes and are not checked, need to check if the field doesn't exist
        // and set it to false in this case
        if ($request->has('published') === false) {
            $requestData['published'] = "0";
        }

        if ($request->has('premium') === false) {
            $requestData['premium'] = "0";
        }

        $data = $this->validator($requestData);

        if (isset($data['image']) === true) {
            // 'image' is the name of the field input in which we stored a file
            // 'photos' is the name of the directory in which we will store given file: from config/filesystems.php: storage/app/public,
            // in this case it will be: storage/app/public/photos, as we called the dir this way
            $path = $request->file('image')->store('photos');
            $data['image'] = $path;
        }

        $post->update($data);

        if (isset($data['tags']) === true) {
            // this method will get the string with tags, example: '#style #life #adventures' and split those by whitespaces,
            // create a tag instance out of each one of those.
            $tagIds = TagsParsingService::parse($data['tags']);

            // this code will get all tags in relation with this post and replace them all with new tags. if tags was an array:
            // [1, 2] and we gave it [1], it would just remove '2' from the array
            $post->tags()->sync($tagIds);
        }

        if (isset($data['image']) === true) {
            Storage::delete($oldImage);
        }

        return redirect(route('admin.post.edit', $post->id))->with('message', 'Successfully updated the post');
    }

    /**
     * @param $id
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy($id)
    {
        /**
         * @var Post $post
         */
        $post = Post::findOrFail($id);

        $post->delete();

        // if you wont specify the disc, the disc from which we delete data is the default one: in our case storage/app/public
        Storage::delete($post->image);

        return redirect('/')->with('message', 'Successfully deleted the post');
    }
}
