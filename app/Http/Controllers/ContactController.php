<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Services\ContactFormService;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index() {
        return view('pages.contact');
    }

    public function send(ContactFormRequest $contactFormRequest, ContactFormService $contactFormService) {
        $contactFormData = $contactFormRequest->validated();

        $contactFormService->sendContactEmail($contactFormData);

        return redirect(route('posts'))->with('message', 'Successfully sent the contact form');
    }
}
