<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Models\Post;

class PostController extends Controller
{
    /**
     * @return Factory|View
     */
//    public function index() {
//        $posts = Post::latest('date)->get();
////        return view('pages.posts', [
////            'posts' => $posts
////        ]);
////        or
//        return view('pages.posts', compact('posts'));
//    }

    public function index() {
        // published() is the method 'scopePublished' I added in Post model. We can define those scope methods to use in querying data.
        // the method receives query and can modify it.
        // in scopePublished i specified, that i only want posts with 'published' field set to 1.
        $posts = Post::published()->with(['tags', 'author'])->latest('date')->paginate(3);

        return view('pages.posts', compact('posts'));
    }

//    // in case we pass id to url we can retrieve it this way:
//    public function show($id) {
////        $post = Post::find($id); // zwróci jeden element modelu Post o podanym id
////        $post = Post::where('id', $id)->first(); // zwroci jeden, pierwszy element zamiast kolekcji
////        $post = Post::where('id', $id)->get(); // zwroci kolekcje, na kolekcji trzebaby bylo iterowac, aby dostac sie do konkretnego posta
////        $post = Post::whereId($id)->first(); // zwroci kolekcje szukajac kolumny 'id' o wartosci $id
////        $post = Post::where('post_id', $id)->first(); // it will NOT throw any errors, just pass null into the variable when post is not found
//        $post = Post::where('post_id', $id)->firstOrFail(); // will throw a 404 page when post is not found, then the check below is not needed
//
////        if (is_null($post) === true) {
//////            die('this post doesnt exist'); // we can either do whatever we want to do when post is not existant, or
////            return abort(404); // which will return a 404 default error page
////        }
//
//        return view('pages.post', compact('post'));
//    }

//    /**
//     * @param Post $post
//     * @return Factory|View
//     */
    // in case we use laravel casting in url: 'posts/{post}' instead of 'posts/{post_id}' we can type hint laravel and get the
    // entity in method's arguments:
//    public function show(Post $post) {
//        return view('pages.post', compact('post'));
//    }

//    // showing post using its slug instead of its id
//    public function show(string $slug) {
////        $post = Post::where('slug', $slug)->firstOrFail();
//        $post = Post::whereSlug($slug)->firstOrFail();
//
//        return view('pages.post', compact('post'));
//    }

    public function show($slug) {
//        $post = Post::published()->where('slug', $slug)->firstOrFail();
        // or
        $post = Post::published()->whereSlug($slug)->firstOrFail();

        return view('pages.post', compact('post'));
    }
}
