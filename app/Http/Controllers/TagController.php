<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function show($slug) {
        $posts = Post::published()
            // whereHas('tags', function($query) will refer to tags table, will present you with a query that will be run on tags
            // associated with posts in current query
            ->whereHas('tags', function($query) use ($slug) {
                $query->whereSlug($slug);
            })
            ->latest('date')
            ->paginate(3);

        return view('pages.posts', compact('posts'));
    }
}
