<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CommentController extends Controller
{
    public function store(Request $request) {
        $data = $request->validate([
            'post_id' => 'required|numeric|exists:posts,id',
            'content' => 'required|min:3',
        ]);

        $data = Arr::add($data, 'user_id', $request->user()->id);

        Comment::create($data);

        return back()->with('message', 'Successfully added the comment');
    }
}
