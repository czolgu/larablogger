<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SearchController extends Controller
{
    protected function validator($data) {
        return Validator::make($data, [
            'q' => 'required|min:3',
        ])->validate();
    }

    public function index(Request $request) {
        $query = $this->validator($request->all());

        $posts = Post::published()
            ->where('title', 'like', "%{$query['q']}%")
            ->paginate(3);

        $posts->appends(['q' => $query['q']]);

        return view('pages.posts', compact('posts'));
    }
}
