<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    // setting it to public allowes us to return a markdown with this variable, no need to manually pass it to the markdown
    // all variables declared here as public are going to be available in the markdown
    public $contactFormData;

    /**
     * ContactForm constructor.
     * @param array $contactFormData
     */
    public function __construct(array $contactFormData)
    {
        $this->contactFormData = $contactFormData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contact-form');
    }
}
